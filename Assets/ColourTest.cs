﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourTest : MonoBehaviour
{

    public Material materialB;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            ChangeColour();
        }
    }

    public void ChangeColour()
    {
        Debug.Log("Change colour called");
        Color col = new Color(UnityEngine.Random.Range(0.0f, 1.0f), UnityEngine.Random.Range(0.0f, 1.0f), UnityEngine.Random.Range(0.0f, 1.0f));
        Debug.Log("Color selectes is :" + col);
        GetComponent<Renderer>().material = materialB;
    }

}
