﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

//This class allows you to cycle through scenes and cycle through timelines in the interest of testing
public class PrototypeController : MonoBehaviour
{
    private MLInputController _controller;
    private int timeLineIndex = -1;
    private bool triggerRelased = false;

    [Tooltip("You can cycle through any timelines that are in this list")]
    public List<PlayableDirector> timelines;

    void Awake()
    {
        MLInput.Start();
        MLInput.OnControllerButtonDown += OnButtonDown;
        MLInput.OnControllerButtonUp += OnButtonUp;
        _controller = MLInput.GetController(MLInput.Hand.Left);
    }

    void OnDestroy()
    {
        MLInput.OnControllerButtonDown -= OnButtonDown;
        MLInput.OnControllerButtonUp -= OnButtonUp;
        MLInput.Stop();
    }

    void Update()
    {
        ListenForControl();
    }

    /// <summary>
    /// Listens for trigger input
    /// </summary>
    private void ListenForControl()
    {
        if(_controller.TriggerValue ==1 && triggerRelased)
        {
            triggerRelased = false;
            LoadNextScene();
        }

        if(_controller.TriggerValue == 0) //Reset the trigger released value so you cant hold down the trigger and rapidly scycle through scenes
        {
            triggerRelased = true;
        }
    }

    void OnButtonDown(byte controller_id, MLInputControllerButton button)
    {
         if(button == MLInputControllerButton.Bumper) //Next time line
        {
            CycleTimelines();
        }else if(button == MLInputControllerButton.HomeTap) //Previous Scene
        {
            LoadSetupScene();
        } 
    }

    void OnButtonUp(byte controller_id, MLInputControllerButton button)
    {

    }

    /// <summary>
    /// Cycles through the activeTimelines and enables a timeline while disabling the others
    /// </summary>
    public void CycleTimelines()
    {
        timeLineIndex++;

        if(timeLineIndex > timelines.Count-1)
        {
            timeLineIndex = 0;
        }

        StartCoroutine(TurnOffTimelines(timeLineIndex));

        
    }

    IEnumerator TurnOffTimelines(int i)
    {
       if(timelines.Count == 0)
        {
            Debug.LogError("You have not added any timelines to the 'timeslines' list on the PrototypeController script");
            yield return null;
        }
        foreach(PlayableDirector pd in timelines)
        {
            if (timelines[i] != pd)
            {
                pd.time = 0.0f;
            }
        }

        yield return new WaitForEndOfFrame();
        foreach (PlayableDirector pd in timelines)
        {
            if (timelines[i] != pd)
            {
                pd.gameObject.SetActive(false);
            }
            else
            {
                pd.gameObject.SetActive(true);
            }
        }
    }

    /// <summary>
    /// Loads the next scene, skipping the build settings scene, and looping back around if surpassing the end of the scenes list
    /// </summary>
    public void LoadNextScene()
    {
        int totalScenes = SceneManager.sceneCountInBuildSettings-1;

        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int newSceneIndex = currentSceneIndex + 1;

        if (newSceneIndex <= totalScenes)
        {
            SceneManager.LoadScene(newSceneIndex);
        }
        else {
            SceneManager.LoadScene(1); //Loads scene 1 to skip the set up scene
        }
    }

    /// <summary>
    /// Lodas the first scene in the build whch should be the build setup scene
    /// </summary>
    void LoadSetupScene()
    {
        SceneManager.LoadScene(0);
    }

    //public void LoadPreviousScene()
    //{
    //    int totalScenes = SceneManager.sceneCountInBuildSettings-1;

    //    int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    //    int newSceneIndex = currentSceneIndex - 1;
    //    if (newSceneIndex >= 0)
    //    {
    //        SceneManager.LoadScene(newSceneIndex);
    //    }
    //    else
    //    {
    //        SceneManager.LoadScene(totalScenes);
    //    }
    //}


}