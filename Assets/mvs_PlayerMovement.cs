﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using UnityEngine.Video;
using UnityEngine.Networking;
using UnityEngine.XR.MagicLeap;

public class mvs_PlayerMovement : NetworkBehaviour
{
    Renderer ren;
    public Material materialRed;
    public Material materialBlue;
    bool isRed = true;
    Material currentMat = null;
    private VideoPlayer videoPlayer;
    private MLInputController _controller;

    float speed = 0.25f;
    // Start is called before the first frame update
    void Start()
    {
        MLInput.Start();
        _controller = MLInput.GetController(MLInput.Hand.Left);
        ren = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Space) || _controller.IsBumperDown)
        {
            if (isLocalPlayer)
            {
                ChangeColour();
            }
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            if (isLocalPlayer)
            {
                ChangeVideoPosition();
            }
        }

        if(Input.GetKeyDown(KeyCode.M))
        {
            if (isServer)
            {
                RpcPrintMessage();
            }
            else {
                CmdPrintMessage();
            }
        }
    }

    [ClientRpc]
    private void RpcPrintMessage()
    {
        Debug.Log("I am a client. I have been told to print this message");
    }

    [Command]
    private void CmdPrintMessage()
    {
        Debug.Log("I am the server. I have been told to print this message");
    }


    #region Colour changing

    public void ChangeColour()
    {

        if (isRed)
        {
            isRed = false;
            ren.material = materialBlue;
        }
        else {
            isRed = true;
            ren.material = materialRed;
        }


        if (!isServer)
        {

            CmdChangeCol(isRed);
        }
        else
        {


            RpcChangeCol(isRed);
        }
    }

    [ClientRpc]
    void RpcChangeCol(bool red)
    {
        if(ren == null)
        {
            Debug.Log("REN is null");
            ren = GetComponent<Renderer>();
        }

        if (red)
        {
            ren.material = materialRed;
        } else {
            ren.material = materialBlue;
        }

        
    }

    [Command]
    void CmdChangeCol(bool red)
    {

        if (ren == null)
        {
            Debug.Log("REN is null");
            ren = GetComponent<Renderer>();
        }

        if (red)
        {
            ren.material = materialRed;
        }
        else
        {
            ren.material = materialBlue;
        }

    }

    // void InternalChangeCol

    #endregion

    #region Video sync

    void ChangeVideoPosition()
    {
        long currentFrame = 800;
        videoPlayer.frame = currentFrame;

        if (!isServer)
        {
            CmdChangeVideoPosition(currentFrame + 60);
        }
        else
        {
            RpcChangeVideoPosition(currentFrame + 30);
        }
    }

    [ClientRpc]
    private void RpcChangeVideoPosition(long frame)
    {
        if (videoPlayer == null)
        {
            Debug.Log("Rpc: Videoplayer is null");

            videoPlayer = GameObject.Find("VideoPlayer").GetComponent<VideoPlayer>();
        }
        videoPlayer.frame = frame;
    }

    [Command]
    private void CmdChangeVideoPosition(long frame)
    {
        if (videoPlayer == null)
        {
            Debug.Log("Cmd: Videoplayer is null");
            videoPlayer = GameObject.Find("VideoPlayer").GetComponent<VideoPlayer>();
        }
        videoPlayer.frame = frame;
    }


    #endregion
}
