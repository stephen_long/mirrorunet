﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;
using Mirror;

public class ManageConnection : MonoBehaviour
{
    public NetworkManager networkManager;
    private bool joined = false;
    private MLInputController _controller;

    // Start is called before the first frame update
    void Start()
    {
        MLInput.Start();
        _controller = MLInput.GetController(MLInput.Hand.Left);
    }

    private void OnDestroy()
    {
        MLInput.Stop();
    }

    private void Update()
    {
        if (_controller.TriggerValue > 0.5f || Input.GetKeyDown(KeyCode.H))
        {
            if (joined == false)
            {
                joined = true;
                Debug.Log("Joining as host");
                networkManager.StartHost();

            }
        }

        if (_controller.IsBumperDown || Input.GetKeyDown(KeyCode.C))
        {
            if(joined == false)
            {
                joined = true;
                Debug.Log("Joining as client");
                networkManager.StartClient();

            }
        }
    }
}




