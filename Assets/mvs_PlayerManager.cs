﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.XR.MagicLeap;
using UnityEngine.XR.MagicLeap.Rendering;
using UnityEngine.SpatialTracking;
public class mvs_PlayerManager : NetworkBehaviour
{
    public MagicLeapCamera magicLeapCamera;
    public Camera cam;
    public TrackedPoseDriver trackedPoseDriver;

    private mvs_PlayerMovement playerMovement;

    public override void OnStartLocalPlayer()
    {
        Debug.Log("Will I come first or second");
        playerMovement = GetComponent<mvs_PlayerMovement>();
        playerMovement.enabled = true;
        cam.enabled = true;
        magicLeapCamera.enabled = true;
        trackedPoseDriver.enabled = true;
    }


}
